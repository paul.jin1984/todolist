import React from 'react';
import Header from '../components/Header';
import TodoListContainer from '../containers/TodoListContainer';
import Footer from './Footer';
import { CssBaseline, Container, AppBar, Paper, Toolbar } from '@material-ui/core';

const App = () => {
	return (
		<React.Fragment>
			<AppBar position='sticky'>
				<Toolbar>
					React Todo List
				</Toolbar>
			</AppBar>
			<CssBaseline />
			<Container maxWidth='sm'>
				<Paper style={{ marginTop: 16, marginBottom: 16, padding: 16, paddingBottom: 0 }}>
					<Header />
					<TodoListContainer />
				</Paper>
				<Footer />
			</Container>
		</React.Fragment>
	);
};

export default App;
