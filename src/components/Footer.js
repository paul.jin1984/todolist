/* eslint-disable react/prop-types */
import React from 'react';
import FooterButton from './FooterButton';
import { connect } from 'react-redux';
import { setVisibleOption } from '../actions';
import { ButtonGroup, Paper } from '@material-ui/core';

class Footer extends React.Component {
	state = {
		tasks: this.props.tasks
	};

	handleClick = (term) => {
		this.props.dispatch(setVisibleOption(term));
	}

	render () {
		return (
			<Paper>
				<ButtonGroup style={{ display: 'flex', flexDirection: 'row' }} fullWidth variant='contained' size='small' aria-label='full-width contained primary button group'>
					<FooterButton handleClick={() => this.handleClick('SHOW_ALL')} btnName='Show All' />
					<FooterButton handleClick={() => this.handleClick('SHOW_REMAINING')} btnName='Remaining' />
					<FooterButton handleClick={() => this.handleClick('SHOW_COMPLETED')} btnName='Completed' />
				</ButtonGroup>
			</Paper>
		);
	}
}

const mapStateToProps = (state) => {
	return state;
};

export default connect(mapStateToProps)(Footer);
