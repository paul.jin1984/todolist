/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { addTask } from '../actions';
import { Button, TextField, Grid } from '@material-ui/core';

class Header extends React.Component {
	state = {
		term: ''
	};

	onSubmit = (e) => {
		e.preventDefault();
		if (this.state.term !== '') {
			this.props.dispatch(addTask(this.state.term));
			this.setState({ term: '' });
		}
	}

	render () {
		return (
			<form onSubmit={this.onSubmit} style={{ paddingBottom: 8 }}>
				<Grid container>
					<Grid xs={9} md={10} item style={{ paddingRight: 8 }}>
						<TextField placeholder='Add a task' value={this.state.term} onChange={e => this.setState({ term: e.target.value })} fullWidth />
					</Grid>
					<Grid xs={3} md={2} item>
						<Button color='primary' variant='outlined' type='submit' fullWidth>Add</Button>
					</Grid>
				</Grid>
			</form>
		);
	}
}

const mapStateToProps = (state) => {
	return state;
};

export default connect(mapStateToProps)(Header);
