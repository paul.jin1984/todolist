import { combineReducers } from 'redux';

const defaultTodoList = [{
	id: 1,
	text: 'Create a Todo',
	completed: true
}];

const defaultVisibleOptions = 'SHOW_ALL';

const rootReducer = (state = defaultTodoList, action) => {
	switch (action.type) {
		case 'ADD_TASK':
			return [...state, action.payload];
		case 'DELETE_TASK':
			return state.filter(item => item.id !== action.payload);
		case 'HANDLE_TASK':
			return state.map(item => item.id === action.payload ? { ...item, completed: !item.completed } : item);
		default:
			return state;
	}
};

const visibleReducer = (state = defaultVisibleOptions, action) => {
	switch (action.payload) {
		case 'SHOW_ALL':
			return 'SHOW_ALL';
		case 'SHOW_COMPLETED':
			return 'SHOW_COMPLETED';
		case 'SHOW_REMAINING':
			return 'SHOW_REMAINING';
		default:
			return state;
	}
};

export default combineReducers({
	defaultTodoList: defaultTodoList,
	tasks: rootReducer,
	visibleOption: visibleReducer
});
